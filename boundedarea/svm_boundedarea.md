

```python
import svm
```


```python
from sklearn.svm import SVC, SVR, LinearSVR, LinearSVC
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, ShuffleSplit
from sklearn.metrics import accuracy_score, r2_score, mean_squared_error, explained_variance_score
from sklearn.metrics import make_scorer
from sklearn.multioutput import MultiOutputRegressor

def performance_metric(y_true, y_predict):
    """ Calculates and returns the performance score between 
        true and predicted values based on the metric chosen. """
    
    # TODO: Calculate the performance score between 'y_true' and 'y_predict'
    score = r2_score(y_true, y_predict)
    
    # Return the score
    return score
```


```python
# df = svm.load_dataset("boundedareas_datasetv1_dropped_dims_for_svm.csv")
df = svm.load_dataset("boundedareas_datasetv1_inc_dims_regressor.csv")
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Sl. No.</th>
      <th>Number_of_Windows</th>
      <th>Number_of_Accesses</th>
      <th>MinW1</th>
      <th>MinW2</th>
      <th>MinW3</th>
      <th>MinW4</th>
      <th>MinW5</th>
      <th>A</th>
      <th>A_sq</th>
      <th>Min_Area</th>
      <th>Area_derived</th>
      <th>Volume</th>
      <th>Entity_Class</th>
      <th>Entity_Label</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>2</td>
      <td>1</td>
      <td>1.0</td>
      <td>1.000801</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2.000801</td>
      <td>2.001603</td>
      <td>4.899956</td>
      <td>0.0</td>
      <td>4.041311</td>
      <td>BoundedArea_Within_Layout</td>
      <td>29461.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>3</td>
      <td>1</td>
      <td>1.0</td>
      <td>1.000000</td>
      <td>1.000362</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>3.000362</td>
      <td>3.000725</td>
      <td>4.855455</td>
      <td>0.0</td>
      <td>3.966499</td>
      <td>BoundedArea_Within_Layout</td>
      <td>29461.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>3</td>
      <td>1</td>
      <td>1.0</td>
      <td>1.000000</td>
      <td>1.005365</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>3.005365</td>
      <td>3.010759</td>
      <td>4.863840</td>
      <td>0.0</td>
      <td>4.000115</td>
      <td>BoundedArea_Within_Layout</td>
      <td>29461.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>2</td>
      <td>1</td>
      <td>1.0</td>
      <td>1.012010</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2.012010</td>
      <td>2.024165</td>
      <td>4.867091</td>
      <td>0.0</td>
      <td>4.031940</td>
      <td>BoundedArea_Within_Layout</td>
      <td>29461.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1.0</td>
      <td>1.007693</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2.007693</td>
      <td>2.015446</td>
      <td>4.857930</td>
      <td>0.0</td>
      <td>3.999640</td>
      <td>BoundedArea_Within_Layout</td>
      <td>29461.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
X = pd.get_dummies(df.iloc[:, 1:14])
y = df.iloc[:, 14]
```


```python
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
```


```python
svr = SVR(kernel='poly', C=1.0, gamma=0.3)
```


```python
svc = SVC(kernel='poly', degree=3, gamma=0.3)
```


```python
param_grid = [{
    'C': [0.3, 0.42, 1.0],
    'loss': ['squared_epsilon_insensitive'],
    'epsilon': [0.001, 0.001, 0.01],
    'max_iter': [3500]
}]
cv_sets = ShuffleSplit(n_splits=10, test_size=0.1, random_state=0)
scoring_fnc = make_scorer(performance_metric)
grid = GridSearchCV(linear_svr, param_grid, scoring=scoring_fnc, cv=cv_sets)
```


```python
grid.fit(X_train, y_train)
```




    GridSearchCV(cv=ShuffleSplit(n_splits=10, random_state=0, test_size=0.1, train_size=None),
           error_score='raise',
           estimator=LinearSVR(C=0.42, dual=True, epsilon=0.001, fit_intercept=True,
         intercept_scaling=1.0, loss='epsilon_insensitive', max_iter=3500,
         random_state=None, tol=0.0001, verbose=0),
           fit_params=None, iid=True, n_jobs=1,
           param_grid=[{'loss': ['squared_epsilon_insensitive'], 'C': [0.3, 0.42, 1.0], 'max_iter': [3500], 'epsilon': [0.001, 0.001, 0.01]}],
           pre_dispatch='2*n_jobs', refit=True, return_train_score='warn',
           scoring=make_scorer(performance_metric), verbose=0)




```python
grid.score(X_test, y_test)
```




    0.991685471056426




```python
grid.best_estimator_
```




    LinearSVR(C=0.42, dual=True, epsilon=0.01, fit_intercept=True,
         intercept_scaling=1.0, loss='squared_epsilon_insensitive',
         max_iter=3500, random_state=None, tol=0.0001, verbose=0)




```python
linear_svr = LinearSVR(C=0.42, epsilon=0.01, max_iter=3500, tol=0.0001, loss='squared_epsilon_insensitive')
linear_svr.fit(X_train, y_train)
```




    LinearSVR(C=0.42, dual=True, epsilon=0.01, fit_intercept=True,
         intercept_scaling=1.0, loss='squared_epsilon_insensitive',
         max_iter=3500, random_state=None, tol=0.0001, verbose=0)




```python
# linear_svr.score(X_test, y_test)
linear_svr.score(X_test, y_test)
```




    0.9916722088082149




```python
accuracy_score(linear_svc.predict(X_test), y_test)
```




    0.041084286319356206




```python
import pickle
pickle.dump(grid.best_estimator_, open("best_estimator_linearsvr_2.pkl", "w"))
```


```python
result_df = pd.DataFrame(columns=['y_pred', 'y_true'])
y_pred = linear_svr.predict(X_test)
```


```python
result_df.y_pred = pd.Series(y_pred.astype(np.dtype('float32')))
result_df.y_true = pd.Series(y_test.values.astype(dtype=np.dtype('float32')))
```


```python
result_df.index = X_test.index
```
