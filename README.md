Neighborhood Classification
===========================

The layouts and the bounded area is classified using mass flux as the parameter. The sum of mass flux from every window is equated to the mass flux at the door for all cases, similarly for the layout, the sum of mass flux from the windows within the layout is equated to the mass flux distributed among the access doors or transits of each bounded area

For the layout and the bounded area, the neighborhood classification factors belong to a ratio. The data is performance maximised using Fourier Transform.

The bounded area can only use LinearSVR, but the layout can either use a Multi-Layer Perceptron or LinearSVR as regression algorithms.

For the plan, the ratio between the product of perimeter and moment of the area, and the area is taken to be the neighborhood classification factor.

For the plot, the floor area ratio is taken to be the neighborhood classification factor.

Formulae
========

(1) 

$$
BoundedArea = \frac {ns} {(1 - ns)}
$$
(2) 

$$
Layout = (1 - ns)^2 * (\sum r^2 + \sum r_i r_j)
$$
(3)
$$
PlanBoundedArea = \frac {p * m}{A}
$$
(4)
$$
PlanLayout = \frac {p * m} {A}
$$
(5) 
$$
Plot Layout = F.A.R
$$
