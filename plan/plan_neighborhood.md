

```python
import numpy as np
import skfuzzy as fuzz
import seaborn
```

    /home/aswin/Anaconda3/anaconda3/envs/py36_enscalo/lib/python3.6/site-packages/matplotlib/__init__.py:886: MatplotlibDeprecationWarning: 
    examples.directory is deprecated; in the future, examples will be found relative to the 'datapath' directory.
      "found relative to the 'datapath' directory.".format(key))



```python
def plot():
    plan_layout_area = np.linspace(100, 400, 1000)
    boundedarea_area = np.linspace(2.5, 30.0, 4000)
    p_array = []
    a_array = []
    randno = np.random.randint(4, 8, 100)
    for r in randno:
        sq = np.square(np.random.randn(r))
        p_array.append(sq/np.sum(sq).tolist())

    idx = 0
    for p in p_array:
        result = True
        while result:
            a = np.random.choice(boundedarea_area, size=(len(p)-1))
            result = plan_layout_area[idx] - np.sum(a) <= 0
        a_array.append(a.tolist() + [plan_layout_area[idx]])
        idx += 1
    memfunc = [fuzz.membership.sigmf(np.array(p), 0.5, 0.1) for p in p_array]
    output = [fuzz.defuzzify.centroid(np.array(a_array[idx]), np.array(memfunc[idx])) for idx in range(len(a_array))]
    seaborn.distplot(output)
for i in range(10): plot()
```

    /home/aswin/Anaconda3/anaconda3/envs/py36_enscalo/lib/python3.6/site-packages/scipy/stats/stats.py:1713: FutureWarning: Using a non-tuple sequence for multidimensional indexing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future this will be interpreted as an array index, `arr[np.array(seq)]`, which will result either in an error or a different result.
      return np.add.reduce(sorted[indexer] * weights, axis=axis) / sumval



![png](output_1_1.png)

