

```python
from sklearn.svm import SVC, SVR, LinearSVR, LinearSVC
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, ShuffleSplit
from sklearn.metrics import accuracy_score, r2_score, mean_squared_error, explained_variance_score
from sklearn.metrics import make_scorer
from sklearn.neighbors import RadiusNeighborsRegressor, KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor
from sklearn.preprocessing import Normalizer, StandardScaler, MinMaxScaler, RobustScaler, QuantileTransformer
from sklearn.manifold import Isomap
from sklearn.decomposition import PCA
from sklearn.linear_model import ARDRegression

def performance_metric(y_true, y_predict):
    """ Calculates and returns the performance score between 
        true and predicted values based on the metric chosen. """
    
    # TODO: Calculate the performance score between 'y_true' and 'y_predict'
    score = r2_score(y_true, y_predict)
    
    # Return the score
    return score
```


```python
# df = svm.load_dataset("boundedareas_datasetv1_dropped_dims_for_svm.csv")
df = pd.read_csv('layout_trace_svm_entity_classes_extd_fourier.csv')
```


```python
df = pd.read_csv('layout_trace_temp.csv')
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SequenceID</th>
      <th>LayoutType</th>
      <th>Net_Low_Accessible</th>
      <th>Net_Low_Isolated</th>
      <th>Net_Pre_Plan_Component</th>
      <th>Net_Post_Plan_Component</th>
      <th>Net_High_Isolated</th>
      <th>Net_High_Accessible</th>
      <th>Low_Accessible 1</th>
      <th>Low_Accessible 2</th>
      <th>...</th>
      <th>MinAREA_der_P</th>
      <th>Ader1</th>
      <th>Ader2</th>
      <th>Ader3</th>
      <th>Ader4</th>
      <th>Ader5</th>
      <th>Ader6</th>
      <th>Ader7</th>
      <th>Volume</th>
      <th>Entity_Class</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>Design_Layout</td>
      <td>9.990850</td>
      <td>33.569324</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>43.812071</td>
      <td>32.350388</td>
      <td>9.990850</td>
      <td>0.0</td>
      <td>...</td>
      <td>1.52</td>
      <td>0.00</td>
      <td>1.520000</td>
      <td>25.168961</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>7.263578</td>
      <td>300.164705</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>Open_Plan</td>
      <td>0.000000</td>
      <td>35.114088</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>61.344302</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>...</td>
      <td>1.52</td>
      <td>1.52</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>220.507912</td>
      <td>-1555.847080</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>Partial_Layout</td>
      <td>10.022838</td>
      <td>0.000000</td>
      <td>4.184355</td>
      <td>36.235232</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>10.022838</td>
      <td>0.0</td>
      <td>...</td>
      <td>1.52</td>
      <td>1.52</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>12.079959</td>
      <td>-53.407756</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>Partial_Layout</td>
      <td>0.000000</td>
      <td>53.907315</td>
      <td>4.178629</td>
      <td>35.965189</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>...</td>
      <td>1.52</td>
      <td>1.52</td>
      <td>2.574089</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.574823</td>
      <td>-1555.847080</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>Open_Plan</td>
      <td>0.000000</td>
      <td>35.406083</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>...</td>
      <td>1.52</td>
      <td>1.52</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.014985</td>
      <td>-1555.847080</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 84 columns</p>
</div>




```python
X = pd.get_dummies(df.iloc[:, 1:83])
y = df.iloc[:, 83]
```


```python
X.drop(inplace=True, columns=['MinArea_P', 'MinAREA_der_P'])
```


```python
X.drop(inplace=True, columns=['Has_Hidden', 'Has_Partial', 'Has_Plan_Component', 'Num_Paths', 'Layout_Centroids'])
```


```python
X.drop(inplace=True, columns=['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'Ader1', 'Ader2', 'Ader3', 
                              'Ader4', 'Ader5', 'Ader6', 'Ader7'])
```


```python
X.drop(inplace=True, columns=['Num_Accessible', 'Num_Isolated', 'Num_Plan_Component', 'Num_Windows', 'Num_Accesses'])
```


```python
X.drop(inplace=True, 
       columns=['LayoutType_Design_Layout', 'LayoutType_Hidden_Layout', 'LayoutType_Open_Plan', 'LayoutType_Partial_Layout'])
```


```python
X.drop(inplace=True, columns=['Net_Low_Accessible', 'Net_Low_Isolated', 'Net_Pre_Plan_Component', 'Net_Post_Plan_Component', 
                              'Net_High_Isolated', 'Net_High_Accesssible'])
```


```python
X.drop(inplace=True, 
      columns=['Window_Inclusion 1', 'Window_Inclusion 2', 'Window_Inclusion 3', 'Window_Inclusion 4', 'Window_Inclusion 5', 'Window_Inclusion 6', 'Window_Inclusion 7' , 'Window_Inclusion 8', 
               'Window_Inclusion 9', 'Window_Inclusion 10' , 'Window_Inclusion 11', 'Window_Inclusion 12'])
```


```python
y.head()
```




    0    510
    1   -390
    2   -100
    3   -390
    4   -390
    Name: Entity_Class_Dimension_2, dtype: int64




```python
area = X.loc[:, ['A1', 'A2', 'A3', 'A4', 'A5', 'A6']].values
area_derived = X.loc[:, ['Ader1', 'Ader2', 'Ader3', 'Ader4', 'Ader5', 'Ader6', 'Ader7']].values

area = np.sort(area, axis=1)
for i in range(len(area)):
    if(len(area[i, :][area[i, :] != 0]) != 0):
        area[i, :] = area[i, :] / np.min(area[i, :][area[i, :] != 0])
    else:
        area[i, :] = area[i, :] / np.min(area_derived[i, :][area_derived[i, :] != 0])

area_derived = np.sort(area_derived, axis=1)
for i in range(len(area_derived)):
    if(len(area_derived[i, :][area_derived[i, :] != 0]) != 0):
        area_derived[i, :] = area_derived[i, :] / np.min(area_derived[i, :][area_derived[i, :] != 0])
    else:
        area_derived[i, :] = area_derived[i, :] / np.min(area[i, :][area[i, :] != 0])
```


```python
X.A1 = pd.Series(area[:, 0])
X.A2 = pd.Series(area[:, 1])
X.A3 = pd.Series(area[:, 2])
X.A4 = pd.Series(area[:, 3])
X.A5 = pd.Series(area[:, 4])
X.A6 = pd.Series(area[:, 5])

X['Ader1'] = pd.Series(area_derived[:, 0])
X['Ader2'] = pd.Series(area_derived[:, 1])
X['Ader3'] = pd.Series(area_derived[:, 2])
X['Ader4'] = pd.Series(area_derived[:, 3])
X['Ader5'] = pd.Series(area_derived[:, 4])
X['Ader6'] = pd.Series(area_derived[:, 5])
X['Ader7'] = pd.Series(area_derived[:, 6])
```


```python
normalizer = Normalizer()
X.Net_Low_Accessible = pd.Series(normalizer.fit_transform(X.Net_Low_Accessible.values.reshape(-1,1)).reshape(len(X), ))
X.Net_Low_Isolated = pd.Series(normalizer.fit_transform(X.Net_Low_Isolated.values.reshape(-1,1)).reshape(len(X), ))
X.Net_Pre_Plan_Component = pd.Series(normalizer.fit_transform(X.Net_Pre_Plan_Component.values.reshape(-1,1)).reshape(len(X), ))
X.Net_Post_Plan_Component = pd.Series(normalizer.fit_transform(X.Net_Post_Plan_Component.values.reshape(-1,1)).reshape(len(X), ))
X.Net_High_Isolated = pd.Series(normalizer.fit_transform(X.Net_High_Isolated.values.reshape(-1,1)).reshape(len(X), ))
X.Net_High_Accessible = pd.Series(normalizer.fit_transform(X.Net_High_Accessible.values.reshape(-1,1)).reshape(len(X), ))
```


```python
# X = pd.concat([X, new_df], axis=1)
X.drop(inplace=True, columns=['MinArea', 'MinArea_der'])
# X.drop(inplace=True, columns=['Low_Accessible 1', 'Low_Accessible 2', 'Low_Accessible 3', 'Low_Accessible 4', 'Low_Accessible 5', 'Low_Accessible 6', 'Low_Accessible 7', 'Low_Isolated 1', 'Low_Isolated 2', 'Low_Isolated 3', 'Low_Isolated 4', 'Low_Isolated 5', 'Low_Isolated 6', 'Pre_Plan_Component 1', 'Pre_Plan_Component 2', 'Pre_Plan_Component 3', 'Pre_Plan_Component 4', 'Pre_Plan_Component 5', 'Post_Plan_Component 1', 'Post_Plan_Component 2', 'Post_Plan_Component 3', 'Post_Plan_Component 4', 'Post_Plan_Component 5', 'Post_Plan_Component 6', 'High_Isolated 1', 'High_Isolated 2', 'High_Isolated 3', 'High_Isolated 4', 'High_Isolated 5', 'High_Isolated 6', 'High_Accessible 1', 'High_Accessible 2', 'High_Accessible 3', 'High_Accessible 4', 'High_Accessible 5', 'High_Accessible 6'])
```


```python
X_train, X_test, y_train, y_test = train_test_split(X.astype('float64'), y, test_size=0.33, random_state=42)
```


```python
mlp = MLPRegressor(solver='adam', activation='tanh', hidden_layer_sizes=(400,), 
                   max_iter=5000, beta_1=0.6, beta_2=0.66)
mlp.fit(X_train, y_train)
```




    MLPRegressor(activation='tanh', alpha=0.0001, batch_size='auto', beta_1=0.6,
           beta_2=0.66, early_stopping=False, epsilon=1e-08,
           hidden_layer_sizes=(400,), learning_rate='constant',
           learning_rate_init=0.001, max_iter=5000, momentum=0.9,
           nesterovs_momentum=True, power_t=0.5, random_state=None,
           shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
           verbose=False, warm_start=False)




```python
mlp.score(X_test, y_test)
```




    0.9670915177314892




```python
ard = ARDRegression()
ard.fit(X_train, y_train)
```


```python
ard.score(X_test, y_test)
```


```python
mlp = MLPRegressor(solver='adam', activation='tanh', hidden_layer_sizes=(400,), max_iter=4000, beta_1=0.7, beta_2=0.73)
bagging = BaggingRegressor(base_estimator=mlp, n_jobs=-1)
bagging.fit(X_train, y_train)
```




    BaggingRegressor(base_estimator=MLPRegressor(activation='tanh', alpha=0.0001, batch_size='auto', beta_1=0.7,
           beta_2=0.73, early_stopping=False, epsilon=1e-08,
           hidden_layer_sizes=(400,), learning_rate='constant',
           learning_rate_init=0.001, max_iter=4000, momentum=0.9,
           nesterovs_momentum=True, power_t=0.5, random_state=None,
           shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
           verbose=False, warm_start=False),
             bootstrap=True, bootstrap_features=False, max_features=1.0,
             max_samples=1.0, n_estimators=10, n_jobs=-1, oob_score=False,
             random_state=None, verbose=0, warm_start=False)




```python
bagging.score(X_test, y_test)
```




    0.9847767830678551




```python
mlp = MLPRegressor(solver='adam', activation='tanh', hidden_layer_sizes=(400,), max_iter=4000, beta_1=0.7, beta_2=0.73)
adaboost = AdaBoostRegressor(base_estimator=mlp)
adaboost.fit(X_train, y_train)
```




    AdaBoostRegressor(base_estimator=MLPRegressor(activation='tanh', alpha=0.0001, batch_size='auto', beta_1=0.7,
           beta_2=0.73, early_stopping=False, epsilon=1e-08,
           hidden_layer_sizes=(400,), learning_rate='constant',
           learning_rate_init=0.001, max_iter=4000, momentum=0.9,
           nesterovs_momentum=True, power_t=0.5, random_state=None,
           shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
           verbose=False, warm_start=False),
             learning_rate=1.0, loss='linear', n_estimators=50,
             random_state=None)




```python
adaboost.score(X_test, y_test)
```




    0.9866250865511793




```python
mlp = MLPRegressor()
param_grid = [{
    'solver': ['adam'],
    'activation': ['tanh'],
    'max_iter': [4000],
    'hidden_layer_sizes': [(400,)],
    'beta_1': [0.6, 0.65],
    'beta_2': [0.66, 0.70]
}]
cv_sets = ShuffleSplit(n_splits=10, test_size=0.1, random_state=0)
scoring_fnc = make_scorer(performance_metric)
grid = GridSearchCV(mlp, param_grid, scoring=scoring_fnc, cv=cv_sets, n_jobs=-1)
```


```python
grid.fit(X_train, y_train)
```


```python
grid.best_estimator_
```




    MLPRegressor(activation='tanh', alpha=0.0001, batch_size='auto', beta_1=0.9,
           beta_2=0.999, early_stopping=False, epsilon=1e-08,
           hidden_layer_sizes=(400,), learning_rate='constant',
           learning_rate_init=0.001, max_iter=4000, momentum=0.9,
           nesterovs_momentum=True, power_t=0.5, random_state=None,
           shuffle=True, solver='adam', tol=0.0001, validation_fraction=0.1,
           verbose=False, warm_start=False)




```python
grid.score(X_test, y_test)
```




    0.9868394836783162


